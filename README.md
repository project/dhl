# Drupal Highest Lowest

A POC for testing composer highest/lowest for Drupal core

See the Pipelines link for visuals.

## Contributing
Open an MR.

## Authors and acknowledgment
- Moshe Weitzman based on work at https://www.drupal.org/project/drupal/issues/2976407